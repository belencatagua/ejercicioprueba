/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consolaapp2;

/**
 *
 * @author FACCI
 */
public class Transaccion {
    //Si es tipo deposito, cheque, transferencia, etc
    private String tipoTransaccion;
    
    private double valor;

    public String getTipoTransaccion() {
        return tipoTransaccion;
    }

    public void setTipoTransaccion(String tipoTransaccion) {
        this.tipoTransaccion = tipoTransaccion;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    public double CalcularPorcentaje()
    {
        double resultado=0.0;
        Cuenta cuenta= new Cuenta();
        if(cuenta.getTipoCuenta() == "Ahorros")
        {
            resultado = this.valor * 0.03;
        }
        else
        {
            if (cuenta.getTipoCuenta() == "Plazo Fijo")
            {
                resultado = this.valor * 0.1;
            }
        }
        return resultado;
    }
    
    public void CalcularSaldo()
    {
        //Recordar concepto de objetos y su identidad
        Cuenta cuenta = new Cuenta();
        cuenta.setSaldo(this.valor + CalcularPorcentaje());
        
        //cuenta.setSaldo(this.valor + CalcularPorcentaje() );
    }            
}